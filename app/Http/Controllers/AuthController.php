<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\organizer;
use Session;

class AuthController extends Controller
{
    public function login(Request $res){
        $user = organizer::where(
            [
                ['email',$res->email],
                ['password_hash',md5($res->password)]
            ]
             )->first();
        //dd($user);

        if($user){
        
            session()->put('user',$user);
            // dd(session()->get('user')->name);

            return redirect('/events')->with('message','login success');
        }else{
            return back()->with('message','login fail');
        }

    }

    public function logout(){
       session()->forget('user');
        return redirect('/')->with('message','logout success');
    }
}
