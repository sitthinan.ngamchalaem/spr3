<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['organizer_id','name','slug','date'];
}
